[![](https://images.microbadger.com/badges/version/kchilly/apache-php72-node.svg)](https://microbadger.com/images/kchilly/apache-php72-node "Get your own version badge on microbadger.com")
[![](https://images.microbadger.com/badges/image/kchilly/apache-php72-node.svg)](https://microbadger.com/images/kchilly/apache-php72-node "Get your own image badge on microbadger.com")  
[![dockeri.co](https://dockeri.co/image/kchilly/apache-php72-node)](https://hub.docker.com/r/kchilly/apache-php72-node)

# docker-apache-php72-node
This repository is the source of the `kchilly/apache-php72-node` [docker image](https://hub.docker.com/r/kchilly/apache-php72-node)

## Base
This image is based on `php:7.2-apache` [official docker image](https://hub.docker.com/_/php).

## Environment variables
- **COMPOSER_AUTH**: provides a token for composer to reach private repositories 
