FROM php:7.2-apache

# Container setup arguments
ARG APACHE_DOCUMENT_ROOT="/var/www/html"
ARG COMPOSER_ARGUMENTS="--no-interaction"

# Overrides the apache document root
RUN sed -ri -e "s!/var/www/html!${APACHE_DOCUMENT_ROOT}!g" /etc/apache2/sites-available/*.conf
RUN sed -ri -e "s!/var/www/!${APACHE_DOCUMENT_ROOT}!g" /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# Setup system packages
RUN set -x \
    && apt-get update --quiet \
    && apt-get install --quiet --yes --no-install-recommends \
        git \
        curl \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libpq-dev \
        libmagickwand-dev \
        imagemagick \
        libtidy-dev \
        gnupg1 \
        gnupg2

# Setup Php extensions
RUN docker-php-ext-install -j$(nproc) gd mbstring zip sockets pcntl \
    && pecl install redis apcu imagick-3.4.3RC2 \
    && docker-php-ext-enable imagick \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \
    && docker-php-ext-install pdo pdo_pgsql pgsql pdo_mysql mysqli tidy \
    && docker-php-ext-enable redis \
    && docker-php-ext-enable apcu \
    && docker-php-ext-enable tidy

# Get Composer
RUN curl -ksS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer

# Setting up environment needs
RUN a2enmod rewrite

# Setup nodeJS and Yarn
RUN curl -sL https://deb.nodesource.com/setup_10.x -o /tmp/nodesource_setup.sh \
    && bash /tmp/nodesource_setup.sh \
    && rm /tmp/nodesource_setup.sh \
    && apt-get install --quiet --yes --no-install-recommends nodejs \
    && npm install yarn --global
